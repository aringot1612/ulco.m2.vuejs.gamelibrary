import Home from './components/Home.vue';
import GameDetails from './components/GameDetails.vue';

import VueRouter from 'vue-router';

/** Création du router. */
export default new VueRouter({
  mode: 'history',
  routes: [
    {path: "/", component: Home},
    {path: "/game/:id", component: GameDetails}
  ]
});