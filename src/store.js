import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

/** Création du store. */
const store = new Vuex.Store({
  // Le "state" stocke les différentes variables du store.
  state: {
    count:0,
    offset: 0,
    current: 1,
    limit: localStorage.getItem('limit') != null ? parseInt(localStorage.getItem('limit')) : 12,
    filters: {
      gameModes: [],
      gameGenres: [],
      gameThemes: [],
      gameCategory: 0,
      gameStatus: 0,
      date: '',
      selectedFilterDateType: '',
      filterUpcomingGames: false
    },
    sort: {
      field: 'total_rating_count',
      order: 'desc'
    },
    connected: localStorage.getItem('auth') != null ? 1 : 0,
    identifier: localStorage.getItem('id') != null ? localStorage.getItem('id') : '',
    search: ''
  },
  // Les mutations permettent de créer des "setter" pour chaque variable.
  mutations: {
    setCount(state, value) {
      this.state.count = value;
    },
    setOffset(state, value) {
      this.state.offset = value;
    },
    setCurrent(state, value){
      this.state.current = value;
    },
    setLimit(state, value){
      this.state.limit = value;
    },
    setFilters(state, value){
      this.state.filters = value;
    },
    setSort(state, value){
      this.state.sort = value;
    },
    setSearch(state, value){
      this.state.search = value;
    },
    login() {
      this.state.connected = 1;
    },
    identify(state, id) {
        this.state.identifier = id;
    },
    logout() {
        this.state.connected = 0;
    }
  }
});
export default store;