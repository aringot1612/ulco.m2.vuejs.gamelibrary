import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import router from './router.js';
import Vuex from 'vuex';
import 'vuetify/dist/vuetify.min.css';
import '@mdi/font/css/materialdesignicons.css';
import store from './store';
import colors from 'vuetify/lib/util/colors';

require('dotenv').config();

Vue.config.productionTip = false;
Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(Vuex);

/** Création des thèmes vuetify. */
const opts = {
  theme: {
    options: {
      customProperties: true
    },
    themes: {
      light: {
        primary: colors.blue.darken1,
        secondary: colors.grey.darken1,
        accent: colors.shades.black,
        error: colors.red.accent3,
        background: colors.grey.lighten3,
        info: colors.blue.darken4
      },
      dark: {
        primary: colors.blue.darken2,
        secondary: colors.grey,
        background: '#121212',
        accent: colors.grey.darken1
      }
    },
    dark: true
  },
  icons: {
    iconfont: 'mdi'
  }
};

new Vue({
  vuetify: new Vuetify(opts),
  router: router,
  store,
  render: h => h(App)
}).$mount('#app')