import * as apiService from './apiService';

/** Permet de définir une couverture de jeu 'vide' si ce derner n'en possède pas. */
const emptyCover = {
	url: "https://www.universretail.com/wp-content/themes/consultix/images/no-image-found-360x250.png"
};

/** Renvoie les différents champs de tri. */
export const sortFields = [
	{name: 'By name', value: 'name'},
	{name: 'By user rating value', value: 'rating'},
	{name: 'By user rating count', value: 'rating_count'},
	{name: 'By critic rating value', value: 'aggregated_rating'},
	{name: 'By critic rating count', value: 'aggregated_rating_count'},
	{name: 'By total rating value', value: 'total_rating'},
	{name: 'By total rating count', value: 'total_rating_count'},
	{name: 'By date added to IGDB api', value: 'created_at'},
	{name: 'By first release date', value: 'first_release_date'},
	{name: 'By the number of follows', value: 'follows'},
	{name: 'By the number of follows before release', value: 'hypes'}
];

/** Renvoie les différents ordres de tri. */
export const sortOrders = [
	{name: 'By ascending order', value: 'asc'},
	{name: 'By descending order', value: 'desc'}
];

/** Renvoie les différents filtres par date. */
export const filterDateTypes = [
	{name: 'Filters games being released before that date', value:'<'},
	{name: 'Filters games being released after that date', value:'>'},
	{name: 'Filters games being released on that date', value:'='}
];

/** Renvoie les différentes catégories de jeux. */
export const gameCategories = [
	{name: 'Main game', id: 0},
	{name: 'DLC addon', id: 1},
	{name: 'Expansion', id: 2},
	{name: 'Bundle', id: 3},
	{name: 'Standalone expansion', id: 4},
	{name: 'Mod', id: 5},
	{name: 'Episode', id: 6},
	{name: 'Season', id: 7},
	{name: 'Remake', id: 8},
	{name: 'Remaster', id: 9},
  {name: 'Expanded game', id: 10},
  {name: 'Port', id: 11},
  {name: 'Fork', id: 12}
];

/** Renvoie les différents statuts de jeu. */
export const gameStatus = [
	{name: 'Alpha', id: 2},
	{name: 'Beta', id: 3},
	{name: 'Early access', id: 4},
	{name: 'Offline', id: 5},
	{name: 'Cancelled', id: 6},
	{name: 'Rumored', id: 7},
	{name: 'Delisted', id: 8}
];

/** Fait appel à l'api pour retourner une liste de jeu selon des paramètres spécifiques. */
export async function getGames(search = '', limit = 12, offset = 0, sort = '', filters = ''){
	let data = await apiService.tryGetGames(search, limit, offset, sort, filters);
	// Pour chaque jeu...
	for(let element of data){
		// On modifie la cover pour la rendre plus agréable à afficher.
		handleCover(element);
	}
	return data;
}

/** Fait appel à l'api pour retourner un jeu spécifique. */
export async function getGame(id = 0){
	let data = await apiService.tryGetGame(id);
	// Comme au dessus, même traitement pour la cover.
	handleCover(data);
	// Changement de qualité pour les screenshots et artworks.
	if(data.screenshots != null){
		for(let element of data.screenshots){
			element.url = element.url.replace("t_thumb", 't_1080p');
		}
	}
	if(data.artworks != null){
		for(let element of data.artworks){
			element.url = element.url.replace("t_thumb", 't_1080p');
		}
	}
	return data;
}

/** Récupère tous les modes de jeux pris en charge par l'api. */
export async function getGameModes(){
	let data = await apiService.tryGetFiltersElement('game_modes');
	return data;
}

/** Récupère tous les genres de jeux pris en charge par l'api. */
export async function getGameGenres(){
	let data = await apiService.tryGetFiltersElement('genres');
	return data;
}

/** Récupère tous les thèmes de jeux pris en charge par l'api. */
export async function getGameThemes(){
	let maxLength = 34;
	let data = await apiService.tryGetFiltersElement('themes');
	// Permet de contrôler la taille des thèmes.
	for(let element of data){
		if(element.name === '4X (explore, expand, exploit, and exterminate)'){
			element.name = '4X Explore-Expand-Exploit-Extermin…';
		}
		else if(element.name.length > maxLength){
			element.name = element.name.slice(0, maxLength).concat('…');
		}
	}
	return data;
}

/** Renvoie une chaîne de caractère pour afficher les genres d'un jeu. */
export function getGameGenresForView(genres){
	if(genres != null){
		let genreList = "";
		for(let i = 0 ; i < genres.length ; i++){
			genreList = genreList + genres[i].name + ", ";
		}
		return genreList.slice(0, -2);
	}
	else{
		return "";
	}
}

/** Renvoie une chaîne de caractère pour afficher les thèmes d'un jeu. */
export function getGameThemesForView(themes){
	if(themes != null){
		let themeList = "";
		for(let i = 0 ; i < themes.length ; i++){
			themeList = themeList + themes[i].name + ", ";
		}
		return themeList.slice(0, -2);
	}
	else{
		return "";
	}
}

/** Renvoie une chaîne de caractère pour afficher les plateformes supportées par le jeu. */
export function getGamePlatformsForView(platforms){
	if(platforms != null){
		let platformsList = "";
		for(let i = 0 ; i < platforms.length ; i++){
			platformsList = platformsList + platforms[i].name + ", ";
		}
		return platformsList.slice(0, -2);
	}
	else{
		return "";
	}
}

/** Gére la couverture d'un jeu afin de mieux l'adapter à un affichage. */
function handleCover(element){
	// Si la cover est présente...
	if(element.cover != null){
		if(element.cover.url != null){
			// Permet de retourner une url avec cover de meilleure qualité.
			element.cover.url = element.cover.url.replace("t_thumb", 't_720p');
		}
	}
	// On fournie une cover 'vide'.
	else{
		element.cover = emptyCover;
	}
}