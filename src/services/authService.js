/** Petite fonction permettant de vérifier le login.
 * Seul le mot de passe est pris en compte.
*/
export function authenticate(password){
  return (password === process.env.VUE_APP_PASSWORD);
}