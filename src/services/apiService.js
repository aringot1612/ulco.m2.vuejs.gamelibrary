import axios from "axios";
import store from "../store";

// Client ID et Client Secret pour utiliser l'API.
const clientId = process.env.VUE_APP_CLIENT_ID;
const clientSecret = process.env.VUE_APP_CLIENT_SECRET;
// URL de proxy.
const proxyUrl = process.env.VUE_APP_PROXY_URL;

// Ensemble de champs utilisé par l'api.
const fieldsList = 'id,first_release_date,name,rating,aggregated_rating,aggregated_rating_count,rating,rating_count,hypes,storyline,summary';
const coverSpec = 'cover.url';
const artworskSpec = 'artworks.url';
const screenshotsSpec = 'screenshots.url';
const websitesSpec = 'websites.url,websites.category';
const genreSpec = 'genres.name';
const themeSpec = 'themes.name';
const platformsSpec = 'platforms.name';
// Les champs à utiliser pour avoir quelques informations sur un jeu.
const fieldsListLight = 'fields ' + coverSpec + ',' + genreSpec + ',id,name,summary,url;';
// Les champs à utiliser pour les détails de jeu.
const fieldsListFull = 'fields ' + coverSpec + ',' + genreSpec + ',' + themeSpec + ',' + platformsSpec + ',' + screenshotsSpec + ','+ artworskSpec + ',' + websitesSpec + ',' + fieldsList + ';';

// Récupèration du dernier indicateur de temps pour l'Access token.
let lastTimeStamp = localStorage.getItem('lastTimeStamp');
// Récupèration du nombre de secondes avant expiration du dernier token, selon le lastTimeStamp.
let lastExpiresIn = localStorage.getItem('lastExpiresIn');
// Récupèration du dernier token enregistré.
let lastAccessToken = localStorage.getItem('lastAccessToken');

// Création de la requête afin de récupèrer un access token.
const authCall = {
	method: 'POST',
	url: 'https://id.twitch.tv/oauth2/token?client_id=' + clientId + '&client_secret=' + clientSecret + '&grant_type=client_credentials',
	headers: {
		'Accept': 'application/json'
	}
};

/** Gestion de l'Access token. */
function getAuthCall(){
	// Si le token actuellement enregistré dans le stockage locale est toujours valide...
	if(Math.floor((new Date() - lastTimeStamp)/1000) < lastExpiresIn){
		return lastAccessToken;
	}
	// Sinon, cela signifie qu'il a expiré.
	else{
		// On retourne une promise de résultat contenant le nouveau access token.
		return new Promise(function(resolve, reject){
			try {
				axios.request(authCall).then(response => {
					lastExpiresIn = response.data.expires_in;
					lastTimeStamp = Date.now();
					lastAccessToken = response.data.access_token;
					// Mise à jour du stockage local avec les nouvelles informations.
					localStorage.setItem('lastExpiresIn', lastExpiresIn);
					localStorage.setItem('lastTimeStamp', lastTimeStamp);
					localStorage.setItem('lastAccessToken', lastAccessToken);
					resolve(response.data.access_token);
				});
			} catch (error) {
				reject(error);
			}
		});
	}
}

/** Méthode permettant de récupèrer tous les jeux selon différents paramètres. */
export async function tryGetGames(search, limit, offset, sort, filters){
	// Triage et filtrage par défaut.
	let sortFilter = 'sort total_rating_count desc;';
	let where = 'where total_rating_count != null';
	if(sort != null){
		if((sort.field != null && sort.field != '') && (sort.order != null && sort.order != '')){
			// Si un tri particulier est choisi, on met à jour le sortFilter.
			sortFilter = 'sort ' + sort.field + ' ' + sort.order + ';';
			/** Attention, particularité de l'api :
			 * Il existe des jeux ne bénéficiant pas de tel ou tel champ.
			 * Lorsqu'un champ est null, malheureusement, le tri retourne quand même le jeu en question.
			 * De plus, les jeux concernés se retrouvent en première position en réponse de requête (que l'ordre de tri soit ascendant ou descendant).
			 * Par conséquent, sans application d'un "where" rattaché à chaque "sort" :
			 * -> Les premiers jeux renvoyés sont les jeux ne bénéficiant pas de la valeur à trier...
			 */
			where = 'where '+ sort.field +' != null';
			// Le tri par score - valeur est particulier : le nombre de scores attribués doit être correct, tout comme le score en lui même...
			if(sort.field == 'total_rating'){
				where = where + ' & rating_count != null & rating_count > 0 & aggregated_rating != null & aggregated_rating_count > 0';
			}
			else if(sort.field == 'rating'){
				where = where + ' & rating_count != null & rating_count > 0';
			}
			else if(sort.field == 'aggregated_rating'){
				where = where + ' & aggregated_rating_count != null & aggregated_rating_count > 0';
			}
		}
	}
	// Si le tri par score est activé...
	if(sortFilter.includes('rating')){
		// Si on cherche uniquement les jeux à venir...
		if(filters.filterUpcomingGames){
			/** Le tri doit être modifié car les jeux à venir ne bénéficient pas de score...
			 * On utilise donc la hype du jeu (score avant sortie). */
			sortFilter = 'sort hypes desc;';
			// Si un ordre de tri est transmis, on l'applique.
			if(sort != null){
				if(sort.order != null && sort.order != ''){
					sortFilter = 'sort hypes ' +  sort.order + ';';
				}
			}
			where = 'where hypes != null';
		}
		// Sinon, Si le filtrage par date est activé en mode "sorti le jour sélectionné ou après le jour sélectionné"...
		else if (filters.selectedFilterDateType != '<' && filters.date != ''){
			/** Correctif : Souvent, le filtrage par date ne donne aucun résultat car les jeux sortis très récemment ne sont toujours pas notés.
			Il faut donc changer le where afin d'éviter de cacher les jeux concernés...*/
			where = 'where name != null';
		}
	}
	// Si une recherche est précisée...
	let searchRequest = '';
	if(search != '' && search != null){
		searchRequest = 'search "' + search + '";';
		// On désactive le tri : l'api ne permet pas le tri ET la recherche en même temps.
		sortFilter = '';
		where = 'where name != null';
	}
	// Ajout des filtres.
	if(filters != null && filters != ''){
		if(filters.filterUpcomingGames){
			// Filtre spécial pour les jeux à venir.
			where = where + ' & first_release_date > ' + Math.round(Date.now()/1000);
		}
		else if(filters.selectedFilterDateType != null && filters.date != null){
			// Filtrage par date.
			if(filters.selectedFilterDateType != '' && filters.date != ''){
				where = where + ' & first_release_date ' + filters.selectedFilterDateType + ' ' + Math.round(Date.parse(filters.date) / 1000);
			}
		}
		// Ensemble de filtrage selon les autres paramètres.
		if(filters.gameModes.length > 0){
			where = where + ' & game_modes = [' + filters.gameModes.join(',') + ']';
		}
		if(filters.gameGenres.length > 0){
			where = where + ' & genres = [' + filters.gameGenres.join(',') + ']';
		}
		if(filters.gameThemes.length > 0){
			where = where + ' & themes = [' + filters.gameThemes.join(',') + ']';
		}
		if(filters.gameCategory != null){
			where = where + ' & category =' + filters.gameCategory;
		}
		if(filters.gameStatus != 0){
			where = where + ' & status = ' + filters.gameStatus;
		}
		// On ignore les jeux ne bénéficiant pas de date de sortie si le filtrage est utilisé.
		where = where + '& first_release_date != null;'
	}
	else{
		where =  '';
	}
	// Récupèration de l'access token.
	let access_token = await getAuthCall();
	// Execution de la requête axios avec tous les paramètres.
	return new Promise(function(resolve, reject){
		let call = {
			method: 'POST',
			url: proxyUrl + 'https://api.igdb.com/v4/games',
			headers: {
				'Client-ID': clientId,
				'Authorization': 'Bearer ' + access_token,
				'Accept': 'application/json'
			},
			data: fieldsListLight + searchRequest + sortFilter + where + 'limit ' + limit + ';' + 'offset ' + offset + ';'
		}
		try {
			axios.request(call).then((response) =>{
				// On retourne la réponse.
				resolve(response.data);
				// On stocke le nombre d'éléments concernés par la requête (permet d'ajuster le paginateur).
				store.commit('setCount', response.headers['x-count']);
			});
		} catch (error) {
			reject(error);
		}
	});
}

/** Permet de récupèrer un jeu spécifique par ID. */
export async function tryGetGame(id){
	// Récupèration de l'access token.
	let auth = await getAuthCall();
	// Execution de la requête axios avec l'id de jeu.
	return new Promise(function(resolve, reject){
		let call = {
			method: 'POST',
			url: proxyUrl + 'https://api.igdb.com/v4/games',
			headers: {
				'Client-ID': clientId,
				'Authorization': 'Bearer ' + auth,
				'Accept': 'application/json'
			},
			data: fieldsListFull + 'where id = ' + id + ';'
		}
		try {
			axios.request(call).then((response) =>{
				resolve(response.data[0]);
			});
		} catch (error) {
			reject(error);
		}
	});
}

/** Récupère les différents types d'élements pour nos différents filtres.
 * Tel que les différents genres de jeu par exemple.  */
export async function tryGetFiltersElement(element = ''){
	// Récupèration de l'access token.
	let auth = await getAuthCall();
	// Execution de la requête axios avec le type d'element à renvoyer.
	return new Promise(function(resolve, reject){
		let call = {
			method: 'POST',
			url: proxyUrl + 'https://api.igdb.com/v4/' + element,
			headers: {
				'Client-ID': clientId,
				'Authorization': 'Bearer ' + auth,
				'Accept': 'application/json'
			},
			data: 'fields name; limit 500;'
		}
		try {
			axios.request(call).then((response) =>{
				resolve(response.data);
			});
		} catch (error) {
			reject(error);
		}
	});
}