# Game Library

Student Vue.JS project.

## Live version (hosted on Heroku) : [Click here](https://m2gamelibrary.herokuapp.com/)

## Project report (pdf file) : [Click here](https://gitlab.com/aringot1612/ulco.m2.vuejs.gamelibrary/-/tree/main/report_FR/report.pdf)